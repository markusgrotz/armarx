# The ArmarX Robot Development Environment

This is the umbrella repository for ArmarX packages.

Detailed information about the different packages can be obtained from the [ArmarX documentation](http://armarx.humanoids.kit.edu).

The public mailing list for announcements and new releases can be found at https://www.lists.kit.edu/wws/info/armarx.

## Getting ArmarX

### Packages for Ubuntu 14.04

Please follow the instructions from the [ArmarX Installation documentation](http://h2t-projects.webarchiv.kit.edu/Projects/ArmarX/ArmarXDoc-Installation.html).

### Sourcecode Copy

To obtain your local sourcecode copy of the ArmarX packages you need to execute the following steps:

```bash
git clone https://gitlab.com/ArmarX/armarx.git
cd armarx
git submodule update --init --remote
git submodule foreach 'git checkout master'
```

The first command clones the umbrella Git repository containing all packages as Git submodules.
The second command initializes all submodules and fetches the latest revisions.
The last command checks out the current master branch.

Afterwards follow the instructions for compiling the packages from the [ArmarX documentation](http://h2t-projects.webarchiv.kit.edu/Projects/ArmarX/ArmarXCore-Installation.html#ArmarXCore-Installation-compiling)

## Updating

Updating all ArmarX packages in one command can be done in the following two ways:

1. using *git submodule update*
```
git submodule update --remote
```
This command fetches the master branch of each ArmarX packages and merges it into the currently active branch (even if it is not `master`).
**Using the second update command is probably safer if you are _not_ working on a master branch.**
2. using git *git submodule foreach*
```
git submodule foreach 'git pull'
```
This command runs *git pull* in each package directory.

## SSH Push Access For Project Members

For direct push access to the ArmarX repositories you need to be a member of the ArmarX group on GitLab.
Non members are invited to send us merge requests via GitLab.

Although pushing via HTTPS is possible, it is usually good advice to use [SSH with private keys](https://gitlab.com/help/ssh/README.md) instead.

Run the following command in the `armarx` directory to enable push access via SSH for all ArmarX packages:

```bash
git submodule foreach 'git config remote.origin.pushurl git@gitlab.com:ArmarX/$name.git'
```


## Git Submodule Documentation

Visit the [official submodule documentation](https://git-scm.com/book/en/v2/Git-Tools-Submodules) for further information about Git Submodules.


## Adding new ArmarX Package as Submodule to armarx.git

Perform the following steps to add a new package as a submodule to the armarx.git repository:

```bash
git submodule add -b master https://gitlab.com/ArmarX/NewPackage.git
git config -f .gitmodules submodule.NewPackage.update merge
git add .gitmodules
git commit -m "add NewPackage as submodule: track master branch and use merge as update strategy"
```